
const strToNum = (str)=>{

    if(str == null | str == undefined ){
        return 0
    }
    const number = str.replace(/[$",]/g, "")

    const num = Number(number)
    if (!isNaN(num)){
        return num
    }
    else{
        return 0
    }
    
}

module.exports = strToNum