const obj = {"first_name": 33, "last_name": "SMith"}

const titleCase = (obj)=>{
    if(obj == null | obj == undefined ){
        return ""
   }
   if(typeof obj != "object"){
       return ""
   }
   
    let temp ="";
    for(let idx in obj){
        let temp1 = obj[idx];
        if(!isNaN(temp1)){
            return ""
        }
        temp1 = temp1.toLowerCase();
        temp += temp1[0].toUpperCase() + temp1.slice(1) + " ";
    } 
    return temp;
}

module.exports = titleCase