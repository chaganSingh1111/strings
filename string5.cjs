

const arrToStr =(arr)=>{
    if(arr == null | arr == undefined ){
        return ''
   }
    if(arr.length ==0){
        return ''
    }
    if(!Array.isArray(arr)){
        return ''
    }
    else{
        return arr.toString().replace(/,/g," ")
    }
    
}
module.exports = arrToStr
