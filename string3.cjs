

const printMonth =(date)=>{
    if(date == null | date == undefined ){
        return 'No month'
   }
   if(typeof date != 'string'){
       return 'No month'
   }
   
    const month = date.split('/')
    const getMonth = Number(month[1])
    if(isNaN(getMonth)){
        return 'No month'
       }
    const monthArray  = ['January','Febuary','March','April','May','June','July','August','September','Octomber','November','December']
    return monthArray[getMonth-1]
}

module.exports = printMonth